package services

import (
	"gitlab.com/amanbek.o/mvc/domain"
	"gitlab.com/amanbek.o/mvc/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId)
}
