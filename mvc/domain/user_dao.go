package domain

import (
	"gitlab.com/amanbek.o/mvc/utils"
	"net/http"
)

var users = map[int64]*User{
	13: {
		Id:        13,
		FirstName: "Tleuzhan",
		LastName:  "Mukatayev",
		Email:     "qwert@mail.ru",
	},

	12: {
		Id:        12,
		FirstName: "a",
		LastName:  "b",
		Email:     "c",
	},
}

func GetUser(userId int64) (*User, *utils.ApplicationError) {
	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message: "User not found",
		Status:  http.StatusNotFound,
		Code:    "not_found",
	}
}
